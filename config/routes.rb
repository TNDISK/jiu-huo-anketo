Rails.application.routes.draw do
  devise_for :admin_users, skip: :all
  devise_scope :admin_user do
    get 'admin/login' => 'devise/sessions#new', as: :new_admin_user_session
    post 'login' => 'devise/sessions#create', as: :admin_user_session
    delete 'logout' => 'devise/sessions#destroy', as: :destroy_admin_user_session
  end
  root "questionnaires#new"
  resources :questionnaires, only: %i[new show create destroy]
  resource :admin_users, only: %i[show] do
    get "api" => "admin_users#index"
    get "table" => "admin_users#table"
  end
end
