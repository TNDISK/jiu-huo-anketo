class CreateQuestionnaires < ActiveRecord::Migration[5.2]
  def change
    create_table :questionnaires do |t|
      t.integer :school_year, comment: "学年", null: false
      t.string :university_name, comment: "大学名"

      t.integer :conscious_working, comment: "就活を意識して何か取り組んでいる", null: false
      t.boolean :search_site, comment: "就活サイト等で企業調べをしている(yesの場合)"
      t.boolean :analysis_myself, comment: "自己分析をしている"
      t.boolean :practice_interview, comment: "面接対策をしている"
      t.boolean :practice_es, comment: "ES対策をしている"
      t.boolean :join_internship, comment: "インターンに参加している"
      t.boolean :attend_job_fair, comment: "説明会に参加している"
      t.boolean :interview_company, comment: "企業と面談している"
      t.boolean :selection_by_company, comment: "選考面接を既に受けた"
      t.boolean :other, comment: "その他"

      t.integer :from_now, comment: "これから就活に取り組もうとしている", null: false
      t.boolean :search_site_no, comment: "就活サイト等で企業調べをしている(noの場合)"
      t.boolean :analysis_myself_no, comment: "自己分析をしている(noの場合)"
      t.boolean :practice_interview_no, comment: "面接対策をしている(noの場合)"
      t.boolean :practice_es_no, comment: "ES対策をしている(noの場合)"
      t.boolean :join_internship_no, comment: "インターンに参加している(noの場合)"
      t.boolean :attend_job_fair_no, comment: "説明会に参加している(noの場合)"
      t.boolean :other_no, comment: "その他(noの場合)"

      t.datetime :when_to_start, comment: "いつごろから取り組むつもりか"
      t.boolean :nothing_schedule, comment: "就活する予定はない"

      t.integer :anxiety, comment: "就活に対して不安がある", null: false
      # 不安がある場合
      t.boolean :how_to_do, comment: "就活のやり方がわからない"
      t.boolean :start, comment: "開始時期がわからない"
      t.boolean :prepare, comment: "事前準備がわからない"
      t.boolean :job_offer, comment: "内定をもらえるのか不安"
      t.boolean :nothing_to_do, comment: "自分のやりたいことがない"
      t.boolean :decision, comment: "就職先をなにで決めるべきかわからない"
      t.boolean :talk_interview, comment: "面接で何を話せばいいか"
      t.boolean :write_es, comment: "ESの書き方を知らない"
      t.boolean :just_because, comment: "漠然と不安"
      t.boolean :less_info, comment: "情報が少ない"
      t.boolean :continue_to_work, comment: "内定先が決まってもそこで働き続けられるかわからない"
      t.boolean :unknown_myself, comment: "自己分析ができない"
      t.boolean :other_anxiety, comment: "その他（不安）"
      t.text :description_internship, comment: "インターンに参加した具体的なエピソード(あれば)"
      t.string :consultation, comment: "相談相手について"
      # 不安がない場合

      t.boolean :nothing_anxiety, comment: "もともと不安がない"
      t.boolean :all_right, comment: "周りの人の状況を聞く限り不安に思わない"
      t.boolean :join_internship_reason, comment: "インターンなどに参加した"
      t.boolean :other_no_anxiety, comment: "不安がない理由その他"

      t.integer :most_select_reason, comment: "就活において重要視する点"
      t.timestamps
    end
  end
end
