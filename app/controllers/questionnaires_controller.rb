class QuestionnairesController < ApplicationController
  def new
    @questionnaire = Questionnaire.new
  end

  def create
    @questionnaire = Questionnaire.new(params_questionnaire)
    if @questionnaire.save
      redirect_to @questionnaire
      flash[:notice] = "アンケートを回答しました！ご協力ありがとうございました。"
    else
      render 'new'
    end
  end

  def show; end

  def destroy
    Questionnaire.find(params[:id]).destroy
    flash[:notice] = "削除しました"
    redirect_to table_admin_users_url
  end


  private
  def params_questionnaire
    params.require(:questionnaire).permit(*key_names)
  end

  def key_names
    [
      :school_year,
      :university_name,
      :conscious_working,
      :search_site,
      :analysis_myself,
      :practice_interview,
      :practice_es,
      :join_internship,
      :attend_job_fair,
      :interview_company,
      :selection_by_company,
      :other,
      :from_now,
      :search_site_no,
      :analysis_myself_no,
      :practice_interview_no,
      :practice_es_no,
      :join_internship_no,
      :attend_job_fair_no,
      :other_no,
      :when_to_start,
      :nothing_schedule,
      :anxiety,
      :how_to_do,
      :start,
      :prepare,
      :job_offer,
      :nothing_to_do,
      :decision,
      :talk_interview,
      :write_es,
      :just_because,
      :less_info,
      :continue_to_work,
      :unknown_myself,
      :other_anxiety,
      :description_internship,
      :consultation,
      :nothing_anxiety,
      :all_right,
      :join_internship_reason,
      :other_no_anxiety,
      :most_select_reason,
    ]
  end


end
