class Questionnaire < ApplicationRecord
  enum school_year: { 学部1年: 1, 学部2年: 2, 学部3年: 3, 学部4年: 4, 修士1年: 5, 修士2年: 6, その他: 99 }
  enum conscious_working: { はい: 0, いいえ: 1 }, _suffix: true
  enum from_now: { はい: 0, いいえ: 1 }, _suffix: true
  enum anxiety: { はい: 0, いいえ: 1 }, _suffix: true
  enum most_select_reason: {
    給料:1, 福利厚生:2, 業務内容:3, 今までの経験が活かせるもの:4, 通勤時間:5, 職場の雰囲気:6,
    自分のやりたいことに取り組めるか:7, 企業ブランド:8, 特になし:99
  }
  validates :conscious_working, presence: true
  validates :anxiety, presence: true
  validates :school_year, presence: true
  validates :university_name, length: { maximum: 20 }

  validate :from_now_validation
  # 問③ではいをつけた人(=>問８)も問６が答えられるようになっている
  # =>問５を問３ではいと答えた人に答えられなくするvalidattionを設定
  def from_now_validation
    if from_now.present? && conscious_working == "はい"
      errors.add(:from_now, 'は問３が"いいえ"の人のみ回答してください')
    end
  end
  # Todo: 問８、10のvalidation設定
end
